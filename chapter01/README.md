---
sitle: Introduction
header-includes: |
    \usepackage{amsmath}
    \usepackage{xlop}
---

An **algorithm** is a set of well-defined rules for solving some computational
problems.

# Integer multiplication

In the expression $x \times y = z$, $x$ and $y$ are $n$-digit nonnegative
integers and $z$ is their product.

## Algorithm 1: Grade school algorithm

The performance of this algorithm is measured through the number of *primitive
operations* it performs, as a function of the *number of digits* $n$ in each
input number. The following are counted as primitive operations:

* Adding two single-digit numbers (e.g., $1 + 8$),
* Multiplying two single-digit numbers (e.g., $2 \times 3$), and
* Adding a zero to the beginning or end of a number.

Example: $5678 \times 1234$.

Here, $n = 4$.

\opmul{5678}{1234}\qquad

The grade school algorithm distribute the multiplication over $n$ *partial
products*. A partial product boils down to multiplying each digit of the first
number (e.g., $5678$) by each digit of the second number and adding in carries
as necessary. Thus, $5678 \times 4$, $5678 \times 3$, etc. are all partial
products. Every subsequent partial product is shifted one digit to the left from
the previous partial product. This is akin to adding a $0$ at the end of the
partial product. Thus, for the $i^{th}$ row we add $i-1 0\text{s}$ where $i \in
\{1, 2, 3, \ldots n\}$. For example, to row $1$ we add $1-1 = 0$ zeroes to the
partial product. For row $2$, we add $2-1 = 1$ zeroes (e.g., $17034\mathbf{0}$)
and so on.

The final step is to add up all the partial products, as shown in the
multiplication above.

In general, computing a partial product involves $n$ multiplications (one per
digit) and *at most* $n$ additions (at most one per digit). This results in at
most $n + n = 2n$ primitive operations.

Every partial product requires at most $2n$ operations. There are $n$ partial
products, thus computing all of them requires at most $n \times 2n = 2n^{2}$
primitive operations:

$$
\text{total number of operations} \le \text{constant} \times n^{2}
$$

For a multiplication of two $n$-digit numbers, the work required is $n^{2}$.
If we double the number of digits (i.e., $2n$), then the work required is
$(2n)^{2} = 4n^{2}$ which means the work jumps by factor of $4$. If the number
is quadrupled, the work required jumps by a factor of 16.

**Can we do better?**

## Algorithm 2: Karatsuba multiplication

Example: $5678 \times 1234$

**Steps**:

1. Partition each number into halves and label each half. Thus, we get
$a = 56, b = 78$ for $5678$ and $c = 12, d = 34$ and for $1234$.

2. Compute $a \times c$. Thus $56 \times 12 = 672$.

3. Compute $b \times d$. Thus $78 \times 34 = 2652$.

4. Compute $(a+b) \times (c+d)$. Thus $(56 + 78) \times (12 + 34) = 6164$.

5. Subtract results of steps 2 and 3 from step 4. Thus $6164 - 672 - 2652 = 2840$.

6. Add results of steps 2 (with four trailing zeroes), 3 and 5 (with two
   trailing zeroes). Thus

   $$
   10^{4} \times 672 + 2652 + 10^{2} \times 2840 = 7006652
   $$


## Algorithm 3: Recursive approach

Any number $x$ with an even number $n$ of digits can be expressed in terms of
two $n/2$-digit numbers:

$$
x = a \times 10^{n/2} + b
$$

where $a$ and $b$ are the second half  and first half respectively. For
instance, $456892$ can be expressed as $456 \times 10^{3} + 892$.

Similarly,

$$
y = c \times 10^{n/2} + d \text{.}
$$

Thus, to compute the product of two even-digit numbers $x$ and $y$, we can the
use the expressions above:

$$
x \times y = (a \times 10^{n/2} + b) \times (c \times 10^{n/2} + d) \\
= (a \cdot c) 10^{2n/2} + a \cdot d 10^{n/2} + b \cdot c 10^{n/2} + b \cdot d
$$

**Pseudocode**: `RecIntMult`

> **Input**: Two $n$-digit positive integers $x$ and $y$.
>
> **Output**: The product $x \cdot y$.
>
> **Assumption**: $n$ is a power of $2$.
>
> ```
> if n = 1
>     return x * y
> else
>     (a, b) <- (1st half of x, 2nd half of x)
>     (c, d) <- (1st half of y, 2nd half of y)
>     recursively compute
>           ac <- a * c,
>           ad <- a * d,
>           bc <- b * c, and
>           bd <- b * d
>     return 10^n * ac + 10^n/2 * (ac + bc) + bd
> ```

## Algorithm 4: Karatsuba multiplication (revisited)

Karatsuba multiplication is an optimized version of the `RecIntMult` algorithm
from the previous section. `RecIntMult` uses four recursive calls one for each

$$
(a\cdot c)\cdot 10^n + (a\cdot d + b\cdot c) \cdot 10^{n/2} + b\cdot d
$$

between $n/2$-digit numbers. However we don't really care about neither $a\cdot
d$ nor $b\cdot c$, except inasmuch as we care about their sum $a\cdot d +
b\cdot c$. Given that we only care about three quantities $a\cdot c$, $a\cdot d +
b\cdot c$, and $b \cdot d$, can we get away with only three recursive calls?

**Steps**:

1. Recursively compute $a\cdot c$.

2. Recursively compute $b\cdot d$.

3. Instead of computing $a\cdot d$ and $b\cdot c$ recursively and then summing
   them up, **we'll compute $a + b$ and $d + c$, and recursively compute $(a+b)\cdot
   (d+c)$**. However, $(a\cdot d + b\cdot c) \ne (a + b)\cdot (d + c)$. Thanks
   to Gauss we notice that subtracting the results of the first two steps from
   the result of $(a + b)\cdot (d + c)$ gives up exactly what we want, namely
   $(a\cdot d + b\cdot c)$. It's easy to see with some algebraic manipulation:

   \begin{align*}
   (a + b)\cdot (d + c) - a\cdot c - b\cdot d &= a\cdot d + a\cdot c + b\cdot d
   + b\cdot c - a\cdot c - b\cdot d \\
   &= a\cdot d + b\cdot c \text{.}
   \end{align*}

4. Subtract the results of the first two steps from the result of the third step
   to obtain $a\cdot d + b\cdot c$.

5. Compute $(a\cdot c)\cdot 10^n + (a\cdot d + b\cdot c) \cdot 10^{n/2} + b\cdot d$
    by adding the result of step 1 after adding $10^n$ trailing zeroes, the
    result of step 2, and the result of step 4 after adding $10^{n/2}$ trailing
    zeroes. Thus

    $$
    \underbrace{a\cdot c}_{\text{step 1}} \cdot 10^n
    + \underbrace{[(a+b)\cdot (c+d) - a\cdot c - b\cdot c]}_{\text{step 4}} \cdot 10^{n/2}
    + \underbrace{b\cdot d}_{\text{step 2}}
    $$

Thus Karatsuba multiplication makes only three recursive calls, in contrast to
`RecIntMult`'s four recursive calls.

**Pseudocode**: `Karatsuba`

> **Input**: Two $n$-digit positive integers $x$ and $y$.
>
> **Output**: The product $x \cdot y$.
>
> **Assumption**: $n$ is a power of $2$.
>
> ```
> if n = 1
>     return x * y
> else
>     (a, b) <- (1st half of x, 2nd half of x)
>     (c, d) <- (1st half of y, 2nd half of y)
>     p <- a + b
>     q <- c + d
>     recursively compute
>         ac <- a * c,
>         bd <- b * d, and
>         pq <- p * q
>     compute adbc <- pq - ac - bd
>     return 10^n * ac + 10^n/2 * adbc + bd
> ```

## Algorithm 5: Merge sort

**Sorting**: Given an array of $n$ numbers in arbitrary order, we get an array
of the same numbers, sorted from smallest to largest.

**TODO**: Add example image of MergeSort

**Pseudocode**: `MergeSort`

> **Input**: An array $A$ of $n$ distinct integers.
>
> **Output**: An array with the same integers, sorted from smallest to largest.
>
> ```
> # ignoring base cases
> C <- recursively sort first half of A
> D <- recursively sort second half of A
> return Merge(C, D)
> ```

**Pseudocode**: `Merge`

> **Input**: Sorted arrays $C$ and $D$ (length $n/2$ each).
>
> **Output**: Sorted array $B$ (length $n$).
>
> ```
> i <- 0
> j <- 0
> for 0 ..^ n -> k
>     if C[i] < D[j]
>         B[k] <- C[i]
>         i <- i + 1
>     else
>         B[k] <- D[j]
>         j <- j + 1
> ```

### Analysis

The **running time** of an algorithm is the number of lines of code executed in
a concrete implementation of the algorithm.

In the pseudocode for the `Merge` subroutine, we see that for each of the $l$
iterations there are $4$ primitive operations (one increment of loop index $k$,
one comparison, one assignment, and one increment). This totals $4l + 2  \le 6l$
(Althought $4l + 2$ is a tighter upper-bound, $6l$ is also valid).

> **Theorem 1.2** (Running time of `MergeSort`): For every input array of length
> $n \ge 1$, the `MergeSort` algorithm performs at most $6log_{2}n + 6n$
> operations, where $log_{2}$ denotes the base-$2$ logarithms.

**Proof:**

**TODO**: Add recursion tree image.

Each invocation of `MergeSort` spawns two recursive calls, thus we're dealing
with a binary tree (two children per node).

We account for the work done by `MergeSort` *level by level* based on the
understanding of two things:

1. The number of distint subproblems at a given recursion level.
2. The length of the input provided to each of these subproblems.

In case 1, the work done by a level-$j$ subproblem is just the work done by the
`Merge` subroutine, ignoring the work done by later recursive call. That is,
$6l$ operations where $l$ is the length of the input array to this subproblem.

Remember that $6l$ is the number of operations per subproblem. Given that $n$ is
the length of the original array and $2^j$ is the number of level-$j$
subproblems, then $l = n/2^j$. Substituting it into $6l$, we get $6(\frac{n}{2^j})
= \frac{6n}{2^j}$, which is the work done per a level-$j$ subproblem.

Therefore, the **total work** done by all level-$j$ recursive calls (not
counting later recursive calls) is

$$
\underbrace{2^j}_{\text{number of level-$j$ subproblems}} \times
\underbrace{\frac{6n}{2^j}}_{\text{work done per a level-$j$ subproblem}} = 6n
$$

Thus, $6n$ operations are performed across all the recursive calls at the
$j^{\text{th}}$ level.

However, we're interested in the number of operations performed across all levels
of the recursion tree. We known that a recursion tree, as a function of the
length $n$ of the input array, has $log_{2}n + 1$ levels. For instance, an array
of length $4$ will produce a recursion tree with $log_{2} 4 + 1 = 3$ levels.

**TODO** Add image of example array of length 4's recursion tree

Using our bound of $6n$ operations per level, we can bound the total number of
operations by

$$
\underbrace{(log_{2}n + 1)}_{\text{number of levels}} \cdot \underbrace{(6n)}_{\text{work per level}}
\le 6nlog_{2} + 6n \text{.}
$$

# Guiding principles for the analysis of algorithms

1. Worst-case analysis

This type of analysis gives a running time bound that is valid even for the
worst inputs.

Another alternative approach is *average-case analysis*, which analyzes the
running average time of an algorithm under some assumption about the relative
frequencies of different inputs.

Average-case analysis is useful when you've domain knowledge of the problem. On
the other hand, worst-case analysis, in which you make absolutely no assumptions
about the input, is particularly appropriate for general-purpose subroutines
designed to work well across a range of application domains.

2. Big picture analysis

This principles states that we shouldn't worry about small constant factors
and/or lower-order terms in running-time bounds.

**Justifications**:

1. **Mathematical tractability**. Big picture analysis is way easier
   mathematically than pinning down constant factors or lower-order terms.

2. **Constants depend on environment-specific factors**. Different
   interpretations of the same pseudocode can lead to different constant
   factors and this ambiguity only increases with pseudocode translated into a
   concrete implementation in some high-level programming language, and then
   translated further into machine code.

3. **Lose little predictive power**. We can get away with it since in
   the qualitative  predictions of our mathematical analysis will be highly
   accurate.

4. **Asymptotic analysis**.This principle focues on the rate of growth of an
   algorithm's running time, *as the input size $n$ grows large*. We care more
   about large instances than small ones is because *large problems are the only
   that require algorithm ingenuity*.

# What's a fast algorithm?

> A fast algorithm is an algorithm whose worst-case running time grows slowly
> with the inout size.

The first guiding principle, that we want running time guarantees that do not
assume any domain knowledge, is the reason why we focus on the worst-case
running time of an algorithm.

The second and third guiding principles, that constant factors and that large
problems are the interesting ones, are the reasons why we focus on the rate of
growth of the running time of an algorithm.
