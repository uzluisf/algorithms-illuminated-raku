use v6;

=begin comment
Implement Karatsuba's integer multiplication algorithm in your favorite
programming language. To get the most out of this problem, your program should
invoke the language's multiplication operator only on pairs of single-digit
numbers?

For a concrete challenge, what's the product of the following two 64-digit
numbers?

3141592653589793238462643383279502884197169399375105820974944592

2718281828459045235360287471352662497757247093699959574966967627
=end comment

# Implement the Karatsuba algorithm.
sub karatsuba( \x, \y ) {
    my \n = x.chars;
    if n == 1 {
        return x * y
    }
    else {
        my (\a, \b) = x.substr(0, n div 2), x.substr(n div 2);
        my (\c, \d) = y.substr(0, n div 2), y.substr(n div 2);
        my (\p, \q) = a + b, c + d;

        # recursive calls
        my \ac = karatsuba a, c;
        my \bd = karatsuba b, d;
        my \pq = karatsuba p, q;

        my \adbc = pq - ac - bd;

        return a * c * 10**n + adbc * 10**(n/2) + bd
    }
}

sub evenify( *@numbers --> Array[Str] ) {
    my \m = @numbers.max.chars %% 2
        ?? @numbers.max.chars
        !! @numbers.max.chars + 1;

    Array[Str].new: @numbers.map(-> \n {'0' x (m - n.chars) ~ n});
}

sub product( UInt $x, UInt $y ) {
    # pad numbers with leading zeroes if they're not even characters.
    my (\m, \n) = evenify $x, $y;
    karatsuba m, n;
}

say product
    3141592653589793238462643383279502884197169399375105820974944592,
    2718281828459045235360287471352662497757247093699959574966967627
;

# OUTPUT: 8.539734222673567e+126
