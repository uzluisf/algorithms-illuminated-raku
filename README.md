# Algorithms Illuminated

Notes and Raku adapted code from the [Algorithms
Illuminated](http://algorithmsilluminated.org/) book.

**NOTE**: While these notes might be useful, they're just a distillation
of the material presented in the book. Thus, if you want to get a better and
fuller picture, please read the book.

* [Introduction](/chapter01/README.md)

# License

Artistic License 2.0
